/*
 * @Author: zhuyh zhuyh@sit.com.cn
 * @Date: 2022-12-14 13:34:48
 * @LastEditors: zhuyh zhuyh@sit.com.cn
 * @LastEditTime: 2022-12-19 19:22:24
 * @FilePath: \ko2-demo1\src\api\test\controller.ts
 * @Description: Description
 */

import model from './model';
import * as Utils from '../../components/utils';
import * as koa from 'koa';
import * as koaRouter from 'koa-router';

class Controller extends Utils.Controller  {
  // Gets a list of Models
  public async index(ctx: Utils.BetterContext) {
    
    try {
      let paginateResult = await Utils.paginate(this.model, ctx);
      ctx.status = 200;
      ctx.body = paginateResult;
    } catch (e) {
      ctx.body = e;
      Utils.handleError(ctx, e);
    }
  }

  // Gets a single Model from the DB
  public async show(ctx: Utils.BetterContext) {
    try {
      let entity = await Utils.show(this.model, ctx);
      Utils.validateEntity(entity);
      ctx.status = 200;
      ctx.body = entity;
    } catch (e) {
      Utils.handleError(ctx, e);
    }
  }

  // Creates a new Model in the DB
  public async create(ctx: Utils.BetterContext) {
    try {
      delete ctx.request.body._id;
      let entity = await this.model.create(ctx.request.body)
      ctx.status = 201;
      ctx.body = entity;
    } catch (e) {
      Utils.handleError(ctx, e);
    }
  }

  // Updates an existing Model in the DB
  public async update(ctx: Utils.BetterContext) {
    try {
      delete ctx.request.fields._id;
      let entity = await this.model.findById(ctx.params.id).exec();
      Utils.validateEntity(entity);
      Utils.patchUpdates(entity, ctx.request.fields);
      await entity.save();
      ctx.status = 200;
      ctx.body = entity;
    } catch (e) {
      Utils.handleError(ctx, e);
    }
  }

  // Deletes a Model from the DB
  async destroy(ctx: Utils.BetterContext): Promise<any> {
    try {
      let entity = await this.model.findById(ctx.params.id).exec();
      Utils.validateEntity(entity);
      await entity.remove();
      ctx.status = 204;
      ctx.body = ''
    } catch (e) {
      Utils.handleError(ctx, e);
    }
  }
}

export default new Controller(model)
